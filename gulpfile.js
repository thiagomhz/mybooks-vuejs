var gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),	
	htmlmin = require('gulp-htmlmin'),
	gls = require('gulp-live-server'),
	jshint = require('gulp-jshint'),
	stylish = require('jshint-stylish');	

//default
gulp.task('default', ['html','sass','js','jshint']);

//html minify
gulp.task('html', function() {
  return gulp.src('app/assets/src/html/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./'));
});

//sass concat & minify
gulp.task('sass', function () {
  return gulp.src('app/assets/src/sass/**/*.scss')
    .pipe(concat('style.min.css'))
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('app/assets/dist/css'));
});

//js concat & uglify
gulp.task('js', function () {
  return gulp.src('app/assets/src/js/**/*.js')
  	.pipe(concat('scripts.min.js'))
  	.pipe(uglify())
  	.pipe(gulp.dest('app/assets/dist/js'))
});

//jslint
gulp.task('jshint', function(){
	return gulp.src('app/assets/src/js/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter(stylish));
});

//server & livereload
gulp.task('server', function() {
	var server = gls.static('./', 8000);
	server.start();
	gulp.watch('app/assets/css/**/*.css', apply);
	gulp.watch('app/assets/js/**/*.js', apply);	
	gulp.watch('./*.html', apply);		

	function apply(file){
		server.notify.apply(server,[file]);
	}
});

//watch
gulp.task('watch', ['default','server'], function() {	
	gulp.watch('app/assets/src/sass/**/*.scss', ['sass'])
	gulp.watch('app/assets/src/js/**/*.js', ['js']);	
	gulp.watch('app/assets/src/html/**/*.html', ['html']);		
});






