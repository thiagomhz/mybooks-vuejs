var app = new Vue({
	el: '#app',
	data:{
		books: [],
		mySearch: '',
		orderCol: 'titulo',
		orderInverse: -1,
		pagination: {
			maxItems: 2,
			currentPage: 0,
			totalItems: 0,
			totalPages: 0,			
			list: []
		}
	},
	methods: {
		filterOrderBy: function(e,col){		
			e.preventDefault();	

			this.orderCol = col;
			this.orderInverse = this.orderInverse * -1;
		},
		previous: function(e){
			e.preventDefault();			

			if(this.pagination.currentPage === 0) return false;

			this.pagination.currentPage --;	

			this.books = this.pagination.list[this.pagination.currentPage];						
		},
		next: function(e){
			e.preventDefault();

			if(this.pagination.currentPage === this.pagination.totalPages) return false;

			this.pagination.currentPage ++;

			this.books = this.pagination.list[this.pagination.currentPage];				
		},
		pagePagination: function(e,page){
			e.preventDefault();

			this.pagination.currentPage = page;

			this.books = this.pagination.list[page];			
		}
	},
	ready: function(){
		var self = this;

		self.$http.get('app/assets/data-server.json').then(function(response){						
			self.pagination.totalItems = response.data.length;
			self.pagination.totalPages = Math.ceil(self.pagination.totalItems / self.pagination.maxItems) - 1;

			var arr = [];
			for(var i in response.data){
				arr.push(response.data[i]);						
				if(arr.length >= self.pagination.maxItems){
					self.pagination.list.push(arr);
					arr = [];
				} 
			}
			if(arr.length > 0){
				self.pagination.list.push(arr);
			}

			self.books = self.pagination.list[0];
		});
	}
});